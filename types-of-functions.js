// Function Declaration
function addtion(a, b) {
    console.log(a + b);
}

addtion(30, 50);


// Function Expression
const sum = function (x, y) {
    console.log(x + y);
}

sum(40, 100);


// Arrow Functions
const add = (l, k) => {
    if (l < k) {
        console.log(l + k);
    } else if (l > k) {
        console.log(l - k);
    } else {
        console.log(l * k);
    }
}

add(30, 30);

// Immediately Invoked Fuction Expression
(function myFunc() { // Beginning of function
    const greeting = 'Hello';
    console.log(greeting)
}) // End of function
    () //calls the function execution e.g. myFunc()



const friendlyFunction = (function () {
    let greetCount = 0;
    return function () {
        console.log(`Hello ${greetCount}x`);
        return greetCount++;
    }
})();

friendlyFunction();
friendlyFunction();
friendlyFunction();




// p is a global variable
globalThis.p = 5;
function myFunc() {
  // p is a local variable
  const p = 9;

  function decl() {
    console.log(p);
  }
  const expr = function () {
    console.log(p);
  };
  const cons = new Function("\tconsole.log(p);");

  decl();
  expr();
  cons();
}
myFunc();

// Logs:
// 9 (for 'decl' by function declaration (current scope))
// 9 (for 'expr' by function expression (current scope))
// 5 (for 'cons' by Function constructor (global scope))