setTimeout(function () {
    console.log('Callback as Standard Function');
}, 1000);


//Callback using Arrow Function
setTimeout(() => {
    console.log('Callback as Arrow Function');
}, 1000);



//1 more example function
function exampleAsync(a, b, callback) {
    setTimeout(function () {
        callback(a + b);
    }, 100);
}
console.log('Before asynchronous call');

exampleAsync(2, 3, function (finalresult) {
    console.log('Result: ' + finalresult);
}); console.log('After asynchronous call');





//callback function using Promises
const promise1 = Promise.resolve(5);
const promise2 = 54;
const promise3 = new Promise((resolve) => {
    setTimeout(resolve, 100, 'foo');
});
Promise.all([promise1, promise2, promise3]).then((values) => {
    console.log(values);
});



//Another Promise example
const promiseand1 = Promise.reject(0);
const promiseand2 = new Promise((resolve) => setTimeout(resolve, 100, 'Large'));
const promiseand3 = new Promise((resolve) => setTimeout(resolve, 500, 'Small'));
const promises = [promiseand1, promiseand2, promiseand3];
Promise.all(promises).then((value) => console.log(value)).catch((err) => console.log(err));