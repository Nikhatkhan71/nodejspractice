let i = 2;                  // global variable
function sum() {             // main function & Lexical environment for add function
    console.log('Hello');
    let j = 3;              // global variable for add function
    function add() {        
        console.log(i + j);
    }

    add();
}

sum();


let a = 2;                  
function sum1() {             
    console.log('Hello');
    let j = 3;   
    console.log(j);           
    let addition = function add1() {        
        console.log(a + j);
    }
    return addition;
}

let sum2 = sum1();
sum2();