async function init() {      //async
    await createMovies({ title: `Example to async await`, body: `` });
    getMovies();
}

function createMovies(movie) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            movies.push(movie);
            const error = false;
            if (!error) {
                resolve();
            }
            else {
                reject('Error: Something went wrong!')
            }
        }, 2000);
    })
}


init();
