hello();            // Expressions cannot be called before declaration

let hello = function () {    // It will do the declaration part first i.e. 'let hello' 
    console.log('hello');    
}    

let hello1 = function () {    
    console.log('hello');
} 

hello1();

// To differentiate between global & local scope
let i;                      
for (i = 0; i < 5; i++) {   // Here, i is considered as a global scope so it will print 5 - 5 times
    console.log(i);
}

for (let i = 0; i < 5; i++) {   // Here, i is considered as a local scope so it will print 0-4
    console.log(i);
}

