//Example 1
const promise1 = Promise.resolve(5);
const promise2 = 54;
const promise3 = new Promise((resolve) => {
    setTimeout(resolve, 100, 'foo');
});
Promise.all([promise1, promise2, promise3]).then((values) => {
    console.log(values);
});



//Example 2
const promiseand1 = Promise.reject(0);
const promiseand2 = new Promise((resolve) => setTimeout(resolve, 500, 'Large'));
const promiseand3 = new Promise((resolve) => setTimeout(resolve, 100, 'Small'));
const promises = [promiseand1, promiseand2, promiseand3];
Promise.all(promises).then((value) => console.log(value)).catch((err) => console.log(err));
Promise.any(promises).then((value) => console.log(value)).catch((err) => console.log(err));


